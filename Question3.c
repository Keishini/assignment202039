#include <stdio.h>

int main()
 {
 	int num1,num2,t;//Declare variables
 	printf("Enter first number:");//Ask user to input first number
 	scanf("%d",&num1);//Store value to the first number
 	printf("Enter second number:");//Ask user to input second number
    scanf("%d",&num2);//Store value to the second number
 	t=num1;
 	num1=num2;
 	num2=t;
 	printf("After swapping,num1=%d\n", num1);//Display after swapping first number
 	printf("After swapping,num2=%d\n", num2);//Display after swapping second number
	return 0;
}
