#include <stdio.h>

int main() {
	int r;
	float area;//Declare variables
	printf("Enter radius:");//Ask user to input radius
	scanf("%d",&r);//Store the user input radius value
	area=3.14*r*r;//Calculation of area
	printf("Area of the disk =%f",area);//Display the area
	return 0;
}
